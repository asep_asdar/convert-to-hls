# TASK

## Kebutuhan Sistem
- `Php ^7.3`
- `Laravel ^8.0`
- `mysql`
- `ffmpeg` `ffprobe` & `ffplay`

## Instalasi
- Buat / setup file .env sesuai kebutuhan (Lihat file .env.example untuk contoh)
- jalankan `composer install`
- jalankan `php artisan storage:link`
- jalankan `php artisan migrate:fresh --seed`
- jalankan `php artisan queue:listen` di cmd
- lalu buka tab baru & jalankan `php artisan serve`
- username : `demo` & pass : `demo`
