<?php

if (!function_exists('cache_assets')) {
    /**
     * Cache multiple assets of the same type (css / js only).
     *
     * @param string[] $paths
     * @param string $ext css / js
     * @param boolean $deferJs if js, will add [defer] attribute to resulting <script>
     * @param boolean $noTurbolinksEval if js, will add [data-turbolinks-eval="false"] attribute to resulting <script>
     * @return string
     */
    function cache_assets($paths, $ext, $deferJs = false, $noTurbolinksEval = false)
    {
        if (empty($paths)) {
            return '';
        }

        if (!in_array($ext, ['css', 'js'])) {
            $error = "ERROR: cache_assets function does not recognize {$ext} extension";

            return
                $ext === 'js'
                ? "<!-- {$error} --><script>console.log('{$error}')</script>"
                : "/* {$error} */";
        }


        $cachedUri = hash('fnv164', implode(',', $paths), false) . ".{$ext}";
        $cachedUrl = url("storage/cached-assets/{$cachedUri}");
        $cachePath = storage_path("app/public/cached-assets/{$cachedUri}");

        if (!file_exists($cachePath)) {
            $cacheContent = '';

            foreach ($paths as $path) {
                if (config('app.debug')) {
                    if ($ext === 'js') {
                        $cacheContent .= PHP_EOL . '// file: ' . $path . PHP_EOL;
                    } else if ($ext === 'css') {
                        $cacheContent .= PHP_EOL . '/* file: ' . $path . ' */' . PHP_EOL;
                    }
                }

                $cacheContent .= preg_replace(
                    '/\/\/# sourceMappingURL=.+/',
                    '',
                    file_get_contents(base_path($path))
                );

                if ($ext === 'js') {
                    $cacheContent .= ';' . PHP_EOL;
                }
            }

            file_put_contents($cachePath, $cacheContent);
        }

        if ($ext === 'js') {
            return "<script src=\"{$cachedUrl}\""
                . ($deferJs ? ' defer' : '')
                . ($noTurbolinksEval ? ' data-turbolinks-eval="false"' : '')
                . '></script>';
        }

        return "<link rel=\"stylesheet\" href=\"{$cachedUrl}\">";
    }
}