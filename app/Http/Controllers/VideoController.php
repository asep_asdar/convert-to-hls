<?php

namespace App\Http\Controllers;

use App\Jobs\ConvertVideoToHLS;
use App\Models\MasterVideo;
use FFMpeg\Format\Video\X264;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function test()
    {
        $video = MasterVideo::create([
            "video" => "-",
            "title" => "Test",
            "description" => "description"
        ]);
        $this->dispatch(new ConvertVideoToHLS($video));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('video.data');
    }

    public function show($video = 0)
    {
        $data["video"] = MasterVideo::where("id_video", $video)
        ->where("is_ready", true)->first();
        if(empty($data["video"]))
        {
            return redirect()->route('video.index');
        }

        return view('video.show')->with($data);
    }

    public function create()
    {
        $data["isCreate"] = true;
        return view('video.form')->with($data);
    }

    public function edit($video = 0)
    {
        $data["isCreate"] = false;
        $data["data"] = MasterVideo::find($video);
        if (empty($data["data"])) {
            return redirect()->route('video.index');
        }
        return view('video.form')->with($data);
    }

    public function update($video = 0, Request $req)
    {
        $message = array("message" => "Data tidak valid", "success" => false, "data" => null);
        $code = 400;
        $validator = Validator::make($req->all(), [
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'video' => ['sometimes', 'file', 'mimetypes:video/mp4,video/x-m4v,video/*']
        ]);
        if ($validator->fails()) {
            $message["message"] = $validator->errors()->first();
            return response()->json($message)->setStatusCode($code);
        }
        try {
            $video = MasterVideo::find($video);
            if(empty($video)){
                return response()->json($message)->setStatusCode($code);
            }
            $filename = $video->video;
            $status = true;
            if(!empty($req->hasFile("video"))){
                error_log("masuk sini");
                $extension = $req->video->getClientOriginalExtension();
                $filename = Str::random(16) . "." . $extension;
                $req->video->move(storage_path('videos'), $filename);
                $status = false;
            }

            $video->title = $req->input("title");
            $video->description = $req->input("description");
            $video->video = $filename;
            $video->is_ready = $status;
            if($video->save())
            {
                $message["message"] = "Data video berhasil diperbaharui.";
                $message["success"] = true;
                $code = 200;
                $status ?: $this->dispatch(new ConvertVideoToHLS($video));
            }
        } catch (\Exception $e) {
            error_log("{$e->getFile()} {$e->getLine()} : {$e->getMessage()}");
            Log::error("{$e->getFile()} {$e->getLine()} : {$e->getMessage()}");
        }
        return response()->json($message)->setStatusCode($code);
    }

    public function store(Request $req)
    {
        $message = array("message" => "Data gagal ditambahkan", "success" => false, "data" => null);
        $code = 400;
        $validator = Validator::make($req->all(), [
            'title' => ['required', 'string'],
            'description' => ['required', 'string'],
            'video' => ['required', 'file', 'mimetypes:video/mp4,video/x-m4v,video/*']
        ]);
        if ($validator->fails()) {
            $message["message"] = $validator->errors()->first();
            return response()->json($message)->setStatusCode($code);
        }
        try {
            $extension = $req->video->getClientOriginalExtension();
            $filename = Str::random(16) . "." . $extension;
            $req->video->move(storage_path('videos'), $filename);

            $create = MasterVideo::create([
                "video" => $filename,
                "title" => $req->input("title"),
                "description" => $req->input("description")
            ]);
            if ($create->exists) {
                $this->dispatch(new ConvertVideoToHLS($create));
                $message["message"] = "Video berhasil ditambahkan, silahkan tunggu beberapa saat untuk proses konversi ke HLS";
                $message["success"] = true;
                $code = 200;
            }
        } catch (\Exception $e) {
            error_log("{$e->getFile()} {$e->getLine()} : {$e->getMessage()}");
            Log::error("{$e->getFile()} {$e->getLine()} : {$e->getMessage()}");
        }
        return response()->json($message)->setStatusCode($code);
    }

    public function destroy($video = 0)
    {
        $message = array("message" => "Data gagal dihapus", "success" => false, "data" => null);
        $code = 400;
        try {
            MasterVideo::find($video)->delete();
            $message["message"] = "Data video berhasil dihapus";
            $message["success"] = true;
            $code = 200;
        } catch (\Exception $e) {
            Log::error("{$e->getFile()} {$e->getLine()} : {$e->getMessage()}");
        }

        return response()->json($message)->setStatusCode($code);
    }

    public function ajaxDataTable(Request $req){
        $data = MasterVideo::all();

        return datatables()->of($data)
        ->addColumn("status", function($data){
            return !$data->is_ready ? "<i class='fas fa-video-slash'></i> Sedang Proses" 
            : "<span class='btn btn-info'><i class='fas fa-video'></i> HLS Tersedia</span>";
        })
        ->addColumn("url_show", function($data){
            return route('video.show', ['video' => $data->id_video]);
        })
        ->addColumn("url_edit", function ($data) {
            return route('video.edit', ['video' => $data->id_video]);
        })
        ->addColumn("url_delete", function ($data) {
            return route('video.destroy', ['video' => $data->id_video]);
        })
        ->rawColumns(['status'])
        ->make(true);
    }
}
