<?php

namespace App\Jobs;

use App\Models\MasterVideo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use FFMpeg\Format\Video\X264;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class ConvertVideoToHLS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $video;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MasterVideo $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $lowBitrate = (new X264)->setKiloBitrate(250);
        $midBitrate = (new X264)->setKiloBitrate(500);
        $highBitrate = (new X264)->setKiloBitrate(1000);
        $filename = explode(".", $this->video->video);
        FFMpeg::fromDisk("videos")
        ->open($this->video->video)
        ->exportForHLS()
        ->addFormat($lowBitrate)
        ->addFormat($midBitrate)
        ->addFormat($highBitrate)
        ->toDisk('public')
        ->save("videos/{$filename[0]}.m3u8");

        unlink(storage_path("videos/{$this->video->video}"));
        $this->video->update([
            'is_ready' => true,
            'video' => "{$filename[0]}.m3u8"
        ]);
    }
}
