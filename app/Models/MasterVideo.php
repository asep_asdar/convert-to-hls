<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterVideo extends Model
{
    use SoftDeletes;
    protected $table = 'master_video';
    protected $primaryKey = "id_video";
    protected $fillable = [
        "video",
        "title",
        "description",
        "is_ready"
    ];

    public $timestamps = true;
}
