@extends('layouts.auth')

@section('content')
    <div class="container container-login animated fadeIn">
        <h3 class="text-center">Sign In to Admin Panel</h3>
        <br>
        <div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <em class="fa fa-exclamation-triangle"></em>
                    <strong>Peringatan!</strong> Periksa kembali kombinasi username dan password anda.

                    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all() as $error)
                    <ul class="mb-0" style="padding-inline-start:15px">
                        <li>{{ $error }}.</li>
                    </ul>
                    @endforeach
                </div>
            @endif
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="login-form">
                <div class="form-group form-floating-label">
                    <input id="username" name="username" value="{{ old('username') }}" 
                        type="text" class="form-control input-border-bottom" 
                        autocomplete="username" autofocus>
                    <label for="username" class="placeholder">Username</label>
                </div>
                <div class="form-group form-floating-label">
                    <input id="password" name="password" 
                        autocomplete="current-password"
                        type="password" class="form-control input-border-bottom">
                    <label for="password" class="placeholder">Password</label>
                    <div class="show-password">
                        <em class="icon-eye"></em>
                    </div>
                </div>
                <div class="row form-sub m-0">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox"  class="custom-control-input" type="checkbox" 
                            name="remember" id="remember"
                            {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
                    </div>
                </div>
                <div class="form-action mb-3">
                    <button type="submit" class="btn btn-primary btn-rounded btn-login">Sign In</button>
                </div>
            </div>
        </form>
    </div>

@endsection
