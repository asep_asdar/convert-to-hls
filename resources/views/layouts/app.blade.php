<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Title Page -->
	@hasSection('page.title')
	<title>@yield('page.title') | HLS</title>
	@else
	<title>TASK | HLS</title>
	@endif

	<!-- Fonts and icons -->
	{!! cache_assets([
        'public/assets/js/plugin/webfont/webfont.min.js',
    ], 'js') !!}
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], 
			urls: ['{{ asset("assets/css/fonts.min.css") }}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	{!! cache_assets([
        'public/assets/css/bootstrap.min.css',
        'public/assets/css/atlantis2.css',
        'public/assets/css/sweetalert2.min.css',
        'public/assets/css/demo.css',
    ], 'css') !!}

	@stack('top.styles')
</head>
<body>
    <div class="wrapper">
        @include('partials/header')
		<div class="main-panel">
		    <div class="container">
		        @yield('page.content')
			</div>
		</div>
		@include('partials/footer')
    </div>
    <form id="formLogout" action="{{ url('logout') }}" method="post" style="display:none;"> @csrf </form>

	{!! cache_assets([
        'public/assets/js/core/jquery.3.2.1.min.js',
		'public/assets/js/core/popper.min.js',
		'public/assets/js/core/bootstrap.min.js',
		'public/assets/js/plugin/datatables/datatables.min.js',
		'public/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js',
		'public/assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js',
		'public/assets/js/plugin/select2/select2.full.min.js',
		'public/assets/js/plugin/sweetalert2/sweetalert2.all.min.js',
		'public/assets/js/atlantis2.min.js'
    ], 'js') !!}

	@stack('bottom.scripts')
</body>
</html>
