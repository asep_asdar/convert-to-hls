<div class="main-header" data-background-color="purple">
    <div class="nav-bottom bg-white">
        <h3 class="title-menu d-flex d-lg-none">
            Menu
            <div class="close-menu"> <i class="flaticon-cross"></i></div>
        </h3>
        <div class="container d-flex flex-row">
            <ul class="nav page-navigation page-navigation-secondary">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('video.index') }}">
                        <em class="link-icon icon-film"></em>
                        <span class="menu-title">Data Video</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#logout" onclick="event.preventDefault(); $('#formLogout').submit()">
                        <em class="link-icon icon-logout"></em>
                        <span class="menu-title">Keluar</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
