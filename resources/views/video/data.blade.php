@extends('layouts.app')

@section('page.content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('video.create') }}" class="btn btn-success">
                    <span class="btn-label">
                        <em class="fas fa-plus-circle"></em>
                    </span>
                    Tambah
                </a>
            </div>
            <div class="card-body">
                <table aria-describedby="Data Video" id="table-video" class="full-table display table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Video</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('bottom.scripts')
<script>
    let table = null;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).ready(function(){
        loadList();
    });

    function loadList(){
        table = $('#table-video').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            language: {
                processing: "Memuat data...",
                search: "Pencarian:",
                searchPlaceholder: "Kata pencarian",
            },
            ajax: {
                method: "POST",
                url: "{{ url('/ajax-datatable-video') }}",
            },
            columns: [
                {
                    data: 'title',
                    name: 'title',
                    sortable: true,
                    render: function(data, type, row, meta) {
                        return data;
                    }
                },
                {
                    data: 'description',
                    name: 'description',
                    sortable: true,
                    render: function(data, type, row, meta) {
                        return data;
                    }
                },
                {
                    data: 'status',
                    name: 'status',
                    sortable: false,
                    searchable: false,
                    render: function(data, type, row, meta) {
                        return row.is_ready ? `<a href="${row.url_show}">${data}</a>` : data;
                    }
                },
                {
                    data: 'id_video',
                    name: 'id_video',
                    width: "18%",
                    sortable: false,
                    className: "text-center",
                    render: function(data, type, row, meta) {
                        let edit = '<a href="'+row.url_edit+'"><button data-toggle="tooltip" data-placement="top" data-html="true" title="Edit Data" type="button" class="btn btn-icon btn-round btn-warning">'+
                        '<i class="flaticon-pencil"></i></button></a> ';
                        let del = '<a onclick="confirmRemove(this);" data-id="'+row.url_delete+'" data-title="'+row.title+'"><button data-toggle="tooltip" data-placement="top" data-html="true" title="Hapus Data" type="button" class="btn btn-icon btn-round btn-danger">'+
                        '<i class="far fa-trash-alt"></i></button></a>';
                        return edit + del;
                    }
                },
            ],
            order: [[ 0, "asc" ]],
            lengthMenu: [
                [5, 10, 50, 100],
                [5, 10, 50, 100],
            ],
            drawCallback: function(settings, json) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }

    function confirmRemove(element){
        var id = $(element).attr("data-id");
        var title = $(element).attr("data-title");
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Ingin video dengan judul \""+title+"\"?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#666',
            confirmButtonText: 'Iya',
            cancelButtonText: 'Tidak',
        }).then((result) => {
            if(result.value){
                ajaxRemove(id);
            }
        });
    }

    function ajaxRemove(delId){
        Swal.fire({
            title: "Memproses...",
            text: "Mohon tunggu beberapa saat",
            showConfirmButton: false,
            allowOutsideClick: false
        });
        $.ajax({
            type: "DELETE",
            url: delId,
            dataType: "json",
            contentType: false,
            cache : false,
            processData : false,
            success: function (result){
                Swal.fire("Berhasil", result.message, "success");
                table.ajax.reload();
            },
            error: function (error){
                if(error.status == 400){
                    Swal.fire("Gagal", error.responseJSON.message, "error");
                    return;
                }
                Swal.fire("Gagal", "Server sedang sibuk", "error");
            }
        });
    }
</script>
@endpush