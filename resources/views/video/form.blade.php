@extends('layouts.app')

@section('page.content')
    <div class="col-12">
        <div class="card">
            <form action="#" id="form-video" method="post">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group form-inline">
                                <div class="col-3">
                                    <label for="title" class="col-form-label custom-label">
                                        <em class="text-danger">*</em> Judul
                                    </label>
                                    <em class="text-danger text-small">*Judul video yang akan ditampilkan</em>
                                </div>
                                <div class="col-6">
                                    <input type="text" value="{{ !$isCreate ? $data->title : "" }}" class="form-control full-input" id="title" placeholder="Judul Video">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group form-inline">
                                <div class="col-3">
                                    <label for="description" class="col-form-label custom-label">
                                        <em class="text-danger">*</em> Deskripsi
                                    </label>
                                    <em class="text-danger text-small">*Keterangan tentang isi video</em>
                                </div>
                                <div class="col-6">
                                    <textarea rows="4" class="form-control full-input" id="description" placeholder="Deskripsi Video">{{ !$isCreate ? $data->description : "" }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 section-pg">
                            <div class="form-group form-inline">
                                <div class="col-3">
                                    <label for="video" class="col-form-label custom-label">
                                        <em class="text-danger">*</em> Video
                                    </label>
                                    <em class="text-danger text-small">*Upload video yang ingin anda proses</em>
                                </div>
                                <div class="col-6">
                                    <input {{ $isCreate ? "required" : "" }} type="file" accept="video/mp4,video/x-m4v,video/*" class="form-control full-input" id="video">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" class="btn btn-default">Bersihkan</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('bottom.scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $("#form-video").submit(function(e){
            e.preventDefault();
            Swal.fire({
                title: "Mengirim...",
                text: "Mohon tunggu beberapa saat",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            let method = "{{ $isCreate ? 'POST' : 'PUT' }}";
            let urlAction = "{{ $isCreate ? route('video.store') : route('video.update', ['video' => $data->id_video]) }}";
            let video = document.getElementById("video").files[0];

            let form = new FormData();
            form.append("title", $("#title").val());
            form.append("description", $("#description").val());
            if(video)
                form.append("video", video);

            form.append("_method", method);
            $.ajax({
                type: 'POST',
                url: urlAction,
                data: form,
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                success: function(result) {
                    var content = {};
                    content.message = result.message;
                    content.title = 'Berhasil';
                    content.icon = 'fa fa-check';
                    content.url = "{{ route('video.index') }}";

                    Swal.close();
                    $.notify(content, {
                        type: "success",
                        autohide: true,
                        delay: 3000,
                        placement: {
                            from: "top",
                            align: "right"
                        }
                    });
                    
                    if(method == "POST")
                        $("#form-video").trigger("reset");

                },
                error: function(error) {
                    if (error.status == 400) {
                        Swal.fire("Gagal", error.responseJSON.message, "error");
                        return;
                    }
                    Swal.fire("Gagal", "Maaf server sedang sibuk, silahkan coba lagi nanti.", "error");
                }
            });
        });
    </script>
@endpush