@extends('layouts.app')
@push('top.styles')
    <link href="//vjs.zencdn.net/7.10.2/video-js.min.css" rel="stylesheet">
@endpush

@section('page.content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                {{ $video->title }}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-8">
                        <video id="my-player" class="video-js" controls preload="auto" data-setup='{}'>
                            <source src="/storage/videos/{{ $video->video }}" type="application/x-mpegURL">
                            </source>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a
                                web browser that
                                <a href="https://videojs.com/html5-video-support/" target="_blank">
                                    supports HTML5 video
                                </a>
                            </p>
                        </video>
                    </div>
                    <div class="col-4">{{ $video->description }}</div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('bottom.scripts')
    <script src="//vjs.zencdn.net/7.10.2/video.min.js"></script>
    <script>
        let player = videojs('my-player');
    </script>
@endpush