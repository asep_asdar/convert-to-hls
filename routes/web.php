<?php

use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route as R;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false, 'confirm' => false, 'reset' => false]);
R::group(['middleware' => 'auth'], function () {
    R::get('/', [VideoController::class, 'index']);
    R::get('/test', [VideoController::class, 'test']);
    R::resource('video', VideoController::class);

    R::post('/ajax-datatable-video', [VideoController::class, 'ajaxDataTable']);
});